package cn.fzkj.task;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component("planTask")
public class PlanTask {

    public void generateTask(Integer planId){
        System.out.println("查找plan。。。" + planId);
        System.out.println("创建任务。。。");
        System.out.println("发送任务通知。。。");
        System.out.println(LocalDateTime.now());
    }
}

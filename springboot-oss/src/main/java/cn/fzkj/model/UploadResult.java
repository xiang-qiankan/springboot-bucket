package cn.fzkj.model;


import lombok.Data;
import org.springframework.http.MediaType;

/**
 * @author mr
 * @description
 * @since 2024/4/30
 */
@Data
public class UploadResult {
    private String fileName;
    private String filePath;
    private String key;
    private String thumbPath;
    private String suffix;
    private MediaType mediaType;
    private Long size;

    private Integer width;
    private Integer height;
}

package cn.fzkj.utils;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

/**
 * @ author mr
 * @ description
 * @ since 2024/11/5
 */
public class DateUtils {

    public static String datePath() {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy/MM/dd");
    }
}

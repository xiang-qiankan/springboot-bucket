package cn.fzkj.exception;

/**
 * @author mr
 * @description
 * @since 2024/4/30
 */
public class ImageFormatException extends RuntimeException {

    public ImageFormatException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}

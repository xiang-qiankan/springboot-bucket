package cn.fzkj.exception;

/**
 * @ author mr
 * @ description
 * @ since 2024/11/5
 */
public class BizException extends RuntimeException {

    public BizException(String msg) {
        super(msg);
    }

    public BizException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}

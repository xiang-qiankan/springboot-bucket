package cn.fzkj.config;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * @author mr
 * @description
 * @since 2024/4/30
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "oss")
public class OssProperties {

    private LocalConf local;
    private MinioConf minio;

    public static class LocalConf {
        private String path;

        public LocalConf(String path) {
            this.path = path;
        }

        public LocalConf() {}

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
    }

    public static class MinioConf {
        private String protocol;
        private String bucketName;
        private String region;
        private String source;
        private String domain;
        private String accessKey;
        private String secretKey;
        private String endpoint;

        private String httpsUrl = "https://ffp.scbdyl.cn:8443";

        public String getHttpsUrl() {
            return StringUtils.appendIfMissing(httpsUrl, "/");
        }

        public void setHttpsUrl(String httpsUrl) {
            this.httpsUrl = httpsUrl;
        }

        public String getEndpoint() {
            return StringUtils.appendIfMissing(endpoint, "/");
        }

        public void setEndpoint(String endpoint) {
            this.endpoint = endpoint;
        }

        public String getNonNullProtocol() {
            return Optional.ofNullable(protocol).orElse("http");
        }

        public void setProtocol(String protocol) {
            this.protocol = protocol;
        }

        public String getNonNullBucketName() {
            return Optional.ofNullable(bucketName).orElse("system-bucket");
        }

        public void setBucketName(String bucketName) {
            this.bucketName = bucketName;
        }

        public String getNonNullRegion() {
            return Optional.ofNullable(region).orElse("ap-chengdu");
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getNonNullSource() {
            return Optional.ofNullable(source).orElseGet(() -> {
                LocalDate now = LocalDate.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
                return formatter.format(now);
            });
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getNonNullDomain() {
            return Optional.ofNullable(domain).orElseThrow(() -> new IllegalArgumentException("oss domain is required."));
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public String getNonNullAccessKey() {
            return Optional.ofNullable(accessKey)
                    .orElseThrow(() -> new IllegalArgumentException("oss accessKey is required."));
        }

        public void setAccessKey(String accessKey) {
            this.accessKey = accessKey;
        }

        public String getNonNullSecretKey() {
            return Optional.ofNullable(secretKey)
                    .orElseThrow(() -> new IllegalArgumentException("oss secretKey is required."));
        }

        public void setSecretKey(String secretKey) {
            this.secretKey = secretKey;
        }
    }
}

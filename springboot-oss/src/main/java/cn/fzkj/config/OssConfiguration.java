package cn.fzkj.config;

import cn.fzkj.helper.FileHelper;
import cn.fzkj.helper.LocalFileHelper;
import cn.fzkj.helper.MinioHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author mr
 * @description
 * @since 2024/4/30
 */
@Configuration
@EnableConfigurationProperties(OssProperties.class)
public class OssConfiguration {

    @Bean
    public FileHelper fileHelper(@Autowired OssProperties ossProperties) {
        if (ossProperties != null){
            if (ossProperties.getMinio() != null){
                return new MinioHelper(ossProperties.getMinio());
            }
            if (ossProperties.getLocal() != null){
                return new LocalFileHelper(ossProperties.getLocal());
            }
        }
        return new LocalFileHelper(new OssProperties.LocalConf("/home/temp/oss"));
    }
}

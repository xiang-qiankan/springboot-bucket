package cn.fzkj.test;

import cn.fzkj.testcontainer.redis.Cache;
import cn.fzkj.testcontainer.redis.RedisBackedCache;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.AliasFor;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.org.apache.commons.io.IOUtils;
import org.testcontainers.utility.DockerImageName;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @DESCRIPTION 测试下test-container
 * @Author yaya
 * @DATE 2023/3/3
 */
//@SpringBootTest
@Testcontainers
public class TestContainerTest {

    private static final DockerImageName REDIS_IMAGE = DockerImageName.parse("redis:6.2.5");

    @ClassRule
    public static GenericContainer<?> redis = new GenericContainer<>(REDIS_IMAGE)
            .withExposedPorts(6379)
            .withAccessToHost(true);

    private Cache cache;


    @Before
    public void setUp() throws Exception {
        String address = redis.getHost();
        Integer port = redis.getFirstMappedPort();
        Integer first = redis.getMappedPort(6379);
//        Integer second = redis.getMappedPort(6380);
        System.out.println("--->");
        System.out.println(address + first);
//        System.out.println(first + "  " + second);
        Jedis jedis = new Jedis(redis.getHost(), redis.getMappedPort(6379));

        cache = new RedisBackedCache(jedis, "test");
    }

    @Test
    public void testFindingAnInsertedValue() {
        cache.put("foo", "FOO");
        Optional<String> foundObject = cache.get("foo", String.class);

        assertThat(foundObject.isPresent()).as("When an object in the cache is retrieved, it can be found").isTrue();
        assertThat(foundObject.get())
                .as("When we put a String in to the cache and retrieve it, the value is the same")
                .isEqualTo("FOO");
    }

    @Test
    public void testNotFindingAValueThatWasNotInserted() {
        Optional<String> foundObject = cache.get("bar", String.class);

        assertThat(foundObject.isPresent())
                .as("When an object that's not in the cache is retrieved, nothing is found")
                .isFalse();
    }

    @Test
    public void testNetwork(){
        try (
                Network network = Network.newNetwork();
                GenericContainer<?> foo = new GenericContainer<>("cirros:0.6")
                        .withNetwork(network)
                        .withNetworkAliases("foo")
                        .withCommand(
                                "/bin/sh",
                                "-c",
                                "while true ; do printf 'HTTP/1.1 200 OK\\n\\nyay' | nc -l -p 8080; done"
                        );
                GenericContainer<?> bar = new GenericContainer<>("cirros:0.6")
                        .withNetwork(network)
                        .withCommand("top")
        ) {
            foo.start();
            bar.start();

            String response = bar.execInContainer("wget", "-O", "-", "http://foo:8080").getStdout();
            assertThat(response).as("received response").isEqualTo("yay");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // 文件映射
    @Nested
    @DisplayName("file and volumes")
    public class FileVolumes {

        @ClassRule
        public GenericContainer<?> redis = new GenericContainer<>(REDIS_IMAGE)
                .withExposedPorts(6379);

        @Test
        public void testFileMapping(){
            String path = "/home/redis/database-export.csv";
            redis.withFileSystemBind("/Users/yaya/Downloads/database-export.csv", path, BindMode.READ_ONLY);
            redis.start();

            try {
                Container.ExecResult command = redis.execInContainer("ls", path);
                assertThat(command.getStdout()).isEqualToNormalizingWhitespace(path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        @Test
        public void testVolumesMapping(){
            String path = "/home/redis/database.txt";
            redis.withClasspathResourceMapping("database.txt", path, BindMode.READ_ONLY);
            redis.start();

            try {
                Container.ExecResult command = redis.execInContainer("cat", path);
                assertThat(command.getStdout()).isEqualTo("hello world!");
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }

    }

    @Nested
    @DisplayName("等待策略")
    public class WaitingStrategy {
        @Test
        @DisplayName("使用http状态码判断容器健康状态")
        public void testHttpCode(){
            GenericContainer nginx = new GenericContainer(DockerImageName.parse("nginx:1.9.4"))
                    .withExposedPorts(80)
                    .waitingFor(Wait.forHttp("/")
                            .forStatusCode(200)
                            .forStatusCodeMatching(code -> code >= 200 && code < 400));
        }


        @Test
        @DisplayName("使用Docker健康检查判断容器健康状态")
        public void testHealthCheck(){
            GenericContainer nginx = new GenericContainer(DockerImageName.parse("nginx:1.9.4"))
                    .withExposedPorts(80)
                    .waitingFor(Wait.forHealthcheck());

        }

        @Test
        @DisplayName("使用容器日志内容判断容器健康状态")
        public void testLogOutput(){
            GenericContainer nginx = new GenericContainer(DockerImageName.parse("nginx:1.9.4"))
                    .withExposedPorts(80)
                    .waitingFor(Wait.forLogMessage(".*Ready.*", 1));
        }

    }

    @Nested
    @DisplayName("启动检查策略")
    public class StartupCheckStrategy {
        // https://java.testcontainers.org/features/startup_and_waits/#wait-strategies
        @Test
        public void watch_doc(){
            System.out.println("https://java.testcontainers.org/features/startup_and_waits/#wait-strategies");
        }
    }



}

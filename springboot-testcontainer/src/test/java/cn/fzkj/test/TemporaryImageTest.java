package cn.fzkj.test;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.images.ImagePullPolicy;
import org.testcontainers.images.PullPolicy;
import org.testcontainers.images.builder.ImageFromDockerfile;

/**
 * 通过Dockerfile创建临时镜像
 * {@link <a href="https://java.testcontainers.org/features/creating_images/">...</a>}
 */
public class TemporaryImageTest {

    @Rule
    public GenericContainer dslContainer = new GenericContainer(
            new ImageFromDockerfile()
                    .withFileFromString("folder/someFile.txt", "hello")
                    .withFileFromClasspath("test.txt", "mappable-resource/test-resource.txt")
                    .withFileFromClasspath("Dockerfile", "mappable-dockerfile/Dockerfile"));


    @Test
    @Ignore
    public void withDsl(){
        new GenericContainer(
                new ImageFromDockerfile()
                        .withDockerfileFromBuilder(builder ->
                                builder
                                        .from("alpine:3.16")
                                        .run("apk add --update nginx")
                                        .cmd("nginx", "-g", "daemon off;")
                                        .build()))
                .withExposedPorts(80);

    }


}

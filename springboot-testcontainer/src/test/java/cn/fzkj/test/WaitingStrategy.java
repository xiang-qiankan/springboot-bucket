package cn.fzkj.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.utility.DockerImageName;

/**
 * 等待策略
 * {@link <a href="https://java.testcontainers.org/features/startup_and_waits/">...</a>}
 */
public class WaitingStrategy {

    @Test
    @DisplayName("使用http状态码判断容器健康状态")
    public void testHttpCode(){
        GenericContainer nginx = new GenericContainer(DockerImageName.parse("nginx:1.9.4"))
                .withExposedPorts(80)
                .waitingFor(Wait.forHttp("/")
                        .forStatusCode(200)
                        .forStatusCodeMatching(code -> code >= 200 && code < 400));
    }

    @Test
    @DisplayName("使用Docker健康检查判断容器健康状态")
    public void testHealthCheck(){
        GenericContainer nginx = new GenericContainer(DockerImageName.parse("nginx:1.9.4"))
                .withExposedPorts(80)
                .waitingFor(Wait.forHealthcheck());

    }

    @Test
    @DisplayName("使用容器日志内容判断容器健康状态")
    public void testLogOutput(){
        GenericContainer nginx = new GenericContainer(DockerImageName.parse("nginx:1.9.4"))
                .withExposedPorts(80)
                .waitingFor(Wait.forLogMessage(".*Ready.*", 1));
    }

    @Test
    public void watch_doc(){
        System.out.println("https://java.testcontainers.org/features/startup_and_waits/#wait-strategies");
    }
}

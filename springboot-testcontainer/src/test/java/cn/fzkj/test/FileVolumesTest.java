package cn.fzkj.test;

import org.junit.ClassRule;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 文件映射
 * {@link <a href="https://java.testcontainers.org/features/files/">...</a>}
 */
public class FileVolumesTest {

    private static final DockerImageName REDIS_IMAGE = DockerImageName.parse("redis:6.2.5");

    @ClassRule
    public GenericContainer<?> redis = new GenericContainer<>(REDIS_IMAGE)
            .withExposedPorts(6379);

    @Test
    public void testFileMapping(){
        String path = "/home/redis/database-export.csv";
        redis.withFileSystemBind("/Users/yaya/Downloads/database-export.csv", path, BindMode.READ_ONLY);
        redis.start();

        try {
            Container.ExecResult command = redis.execInContainer("ls", path);
            assertThat(command.getStdout()).isEqualToNormalizingWhitespace(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testVolumesMapping(){
        String path = "/home/redis/database.txt";
        redis.withClasspathResourceMapping("database.txt", path, BindMode.READ_ONLY);
        redis.start();

        try {
            Container.ExecResult command = redis.execInContainer("cat", path);
            assertThat(command.getStdout()).isEqualTo("hello world!");
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}

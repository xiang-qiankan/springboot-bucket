package cn.fzkj.test;

import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.OutputFrame;
import org.testcontainers.containers.output.ToStringConsumer;
import org.testcontainers.containers.output.WaitingConsumer;
import org.testcontainers.utility.DockerImageName;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.testcontainers.containers.output.OutputFrame.OutputType.STDOUT;

/**
 * 访问容器日志
 * {@link <a href="https://java.testcontainers.org/features/container_logs/">...</a>}
 */
public class AccessLogsTest {

    @ClassRule
    public static GenericContainer redis = new GenericContainer(DockerImageName.parse("redis:6.2.5"))
            .withExposedPorts(6379);

    @Test
    public void testReadingAllLogs(){
        String logs = redis.getLogs(); // stdout and stderr
        System.out.println(logs);

        System.out.println(redis.getLogs(STDOUT)); // stdout

        System.out.println(redis.getLogs(OutputFrame.OutputType.STDERR)); // stderr
    }

    @Test
    public void capturing_output_as_a_string(){
        ToStringConsumer toStringConsumer = new ToStringConsumer();
        redis.followOutput(toStringConsumer, STDOUT);

        String utf8String = toStringConsumer.toUtf8String();

        // Or if the container output is not UTF-8
        String otherString = toStringConsumer.toString(StandardCharsets.ISO_8859_1);

        System.out.println(utf8String);
        System.out.println(otherString);

    }

    @Test
    public void Waiting_for_container_output_to_contain_expected_content(){
        WaitingConsumer consumer = new WaitingConsumer();

        redis.followOutput(consumer, STDOUT);

        try {
            consumer.waitUntil(frame ->
                    frame.getUtf8String().contains("Redis is starting"), 10, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }

    }



}

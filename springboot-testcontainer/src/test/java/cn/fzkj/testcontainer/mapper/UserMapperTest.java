package cn.fzkj.testcontainer.mapper;

import cn.fzkj.testcontainer.entity.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UserMapperTest {

    @Autowired
    UserMapper userMapper;

    public static MySQLContainer mysql = new MySQLContainer("mysql:latest")
        .withPassword("root")
        .withUsername("root")
        .withDatabaseName("user");

    @BeforeAll
    public static void beforeAll(){
        mysql.start();
    }

    @AfterAll
    public static void afterAll(){
        mysql.stop();
    }

    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mysql::getJdbcUrl);
        registry.add("spring.datasource.username", mysql::getUsername);
        registry.add("spring.datasource.password", mysql::getPassword);
    }

//    static class EnvInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
//
//        @Override
//        public void initialize(ConfigurableApplicationContext context) {
//            TestPropertyValues.of(
//                    "spring.datasource.url=jdbc:mysql://localhost:"+mysql.getFirstMappedPort()+"/user",
//                    "spring.datasource.username=root",
//                    "spring.datasource.password=root").applyTo(context);
//        }
//    }

    @Test
    public void testCrud(){
        User user = new User("zhangsan", 18, "成都市");
        // insert
        int insert = userMapper.insert(user);
        assertTrue(insert == 1);
        // update
        user.setUsername("lisi");
        user.setAddress("北京市");
        int update = userMapper.updateById(user);
        assertTrue(update == 1);
        // select
        User user1 = userMapper.selectById(user.getId());
        assertEquals(user1.getUsername(), "lisi");
        assertEquals(user1.getAge(), 18);
        assertEquals(user1.getAddress(), "北京市");
        // delete
        int delete = userMapper.deleteById(user);
        assertTrue(delete == 1);
    }

}
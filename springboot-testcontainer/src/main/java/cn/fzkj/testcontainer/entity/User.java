package cn.fzkj.testcontainer.entity;

import com.baomidou.mybatisplus.annotation.*;

@TableName("t_user")
public class User {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @TableField(value = "user_name")
    private String username;

    private int age;

    private String address;

    @TableLogic(delval = "1")
    private boolean deleted = false;

    @Version
    private Long version = 1L;

    public User() {
    }

    public User(String username, int age, String address) {
        this.username = username;
        this.age = age;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}

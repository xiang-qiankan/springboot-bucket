package cn.fzkj.testcontainer.service.impl;

import cn.fzkj.testcontainer.entity.User;
import cn.fzkj.testcontainer.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl {

    @Autowired
    UserMapper userMapper;

    public int insert(User user){
        return this.userMapper.insert(user);
    }

    public User findUser(int id){
        return this.userMapper.selectById(id);
    }
}

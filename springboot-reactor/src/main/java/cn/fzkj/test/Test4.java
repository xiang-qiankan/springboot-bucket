package cn.fzkj.test;

import reactor.core.publisher.Flux;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ description
 * @ author yaya
 * @ since 2024/8/12
 */
public class Test4 {

    private static class Ranking {
        private Integer index;
        private String name;
        private Integer score;

        public Ranking(String name, Integer score) {
            this.name = name;
            this.score = score;
        }

        public Ranking(Integer index, String name, Integer score) {
            this.index = index;
            this.name = name;
            this.score = score;
        }
        public Integer getIndex() {
            return index;
        }

        public void setIndex(Integer index) {
            this.index = index;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getScore() {
            return score;
        }

        public void setScore(Integer score) {
            this.score = score;
        }

        @Override
        public String toString() {
            return "Ranking{" +
                    "index=" + index +
                    ", name='" + name + '\'' +
                    ", score=" + score +
                    '}';
        }
    }
    public static void main(String[] args) {
        List<Ranking> rankings = List.of(new Ranking("张三", 100)
                , new Ranking("李四", 90),
                new Ranking("王五", 80));
        List<Ranking> block = Flux.fromIterable(rankings)
                .index()
                .map(tuple -> {
                    Long t1 = tuple.getT1();
                    Ranking ranking = tuple.getT2();
                    ranking.setIndex(Math.toIntExact(t1) + 1);
                    return ranking;
                }).collect(Collectors.toList()).block();
        System.out.println(block);
    }
}

package cn.fzkj.test;

import org.springframework.format.annotation.DateTimeFormat;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

public class Test1 {

    public String deviceId;
//    public

    private static String data = "hahah";
    private static int index = 0;

    public static Mono<String> getdata(){
        if (data.length() == 0 ){
            return Mono.empty();
        }
        return Mono.defer(() -> {
//                    index ++;
                    if (index != 3) {
                        throw new RuntimeException("失败了");
                    }
                    return Mono.just(data);
                }).doOnError(err -> System.out.println("err -> " + err))
                .retryWhen(Retry.fixedDelay(10, Duration.ofSeconds(2)))
                .onErrorReturn("不行");
    }


//    public static void main(String[] args) {
//        String input = "elloWorld";
//
//        // 获取字符串的长度
//        int length = input.length();
//
//        // 如果字符串长度小于等于1，则无法拆分
//        if (length <= 1) {
//            System.out.println("字符串长度小于等于1，无法拆分");
//            return;
//        }
//
//        // 计算拆分后数组的长度
//        int arrayLength = (length + 1) / 2;
//
//        // 创建存储拆分后结果的数组
//        String[] result = new String[arrayLength];
//
//        // 从后往前遍历字符串，每次取两个字符为一组放入数组
//        int index = 0;
//        for (int i = length - 1; i >= 0; i -= 2) {
//            int endIndex = i + 1;
//            int startIndex = Math.max(0, i - 1);
//            result[index++] = input.substring(startIndex, endIndex);
//        }
//        Collections.reverse(Arrays.asList(result));
//
//        // 输出结果数组
//        System.out.println("拆分后的数组: " + Arrays.toString(result));
//    }

    public static boolean scheduled() {
        int minutes = 1;
        Flux.interval(Duration.ofSeconds(5))
                .takeWhile(item -> item < minutes * 60 / 5) // 5分钟内最多执行30次
                .doOnNext(item -> {
                    System.out.println(item);
                })
                .subscribeOn(Schedulers.parallel()) // 在并行线程中执行任务
                .doFinally(signalType -> System.out.println("Task completed or cancelled"))
                .subscribe(); // 订阅流，不阻塞主线程
        return true;
    }

    private record Dto(Integer id, String name){}

    public static boolean scheduledfor() {
        int minutes = 1;
        for (int i = 0; i < 5; i++) {
            int finalI = i;
            Dto dto = new Dto(i, "name " + i);
            Flux.interval(Duration.ofSeconds(5))
                    .takeWhile(item -> item < minutes * 60 / 5) // 5分钟内最多执行30次
                    .doOnNext(item -> {
                        System.out.println(dto + " -- " + finalI);
                    })
                    .subscribeOn(Schedulers.parallel()) // 在并行线程中执行任务
                    .doFinally(signalType -> System.out.println("Task completed or cancelled " + finalI))
                    .subscribe(); // 订阅流，不阻塞主线程
        }
        return true;
    }

    public static boolean scheduled11() {
        int minutes = 1;
        int flag = 10;
        Flux.interval(Duration.ofSeconds(5))
                .takeWhile(item -> item < minutes * 60 / 5) // 5分钟内最多执行30次
                .takeUntil(i -> {
                    System.out.println("flag " + i);
                    return i == flag;
                })
                .doOnNext(item -> {
                    System.out.println(item);
                })
                .subscribeOn(Schedulers.parallel()) // 在并行线程中执行任务
                .doOnComplete(() -> System.out.println("Task completed"))
                .doFinally(signalType -> System.out.println("Task completed or cancelled"))
                .subscribe(); // 订阅流，不阻塞主线程
        return true;
    }

    public static void main(String[] args) throws InterruptedException {
        scheduled11();

        Thread.sleep(100000);
    }
}

package cn.fzkj.test;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;

public class Test3 {

    public static void main(String[] args) {
        Integer retentionPeriod = 5; // 分钟
        AtomicBoolean last = new AtomicBoolean(false);
        Flux.interval(Duration.ofSeconds(10))
                .takeWhile(index -> index < retentionPeriod * 60 / 5)
                .doOnNext(index -> {
                    Long logId = 11L;
//                    InspectLogResp logResp = logService.findById(logId);
//                    if (InspectStatus.ON.getStatus().equals(logResp.getInspectStatus())
//                            || retentionPeriod * 60 - (index * 10) <= 0) {
//                        last.set(true);
//                    }else {
//                        long retention = retentionPeriod * 60 - (index * 10);
//                        System.out.println("重新推送消息");
//                    }
                })
                .takeUntil(index -> last.get())
                .doOnError((err) -> {
                    System.out.println("值班查岗推送出现异常");
                })
                .onErrorComplete()
                .subscribeOn(Schedulers.parallel())
                .subscribe();
    }
}

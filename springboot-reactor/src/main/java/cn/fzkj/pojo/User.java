package cn.fzkj.pojo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Table("t_user")
public class User {

    @Id
    private Long id;
    private String userName;
    private String address;
    private boolean deleted;
    private Long version;

    private int age;

}

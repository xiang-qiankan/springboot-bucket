package cn.fzkj.repository;

import cn.fzkj.pojo.User;
import org.springframework.boot.autoconfigure.mongo.ReactiveMongoClientFactory;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends R2dbcRepository<User, Long> {

    Mono<User> findUserByUserName(String name);

    @Query("select * from t_user where address like :address")
    Flux<User> findAllByAddress(String address);

}

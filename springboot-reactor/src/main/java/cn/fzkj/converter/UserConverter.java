package cn.fzkj.converter;

import cn.fzkj.pojo.User;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class UserConverter implements Converter<Row, User> {

    @Override
    public User convert(Row source) {
        User user = new User();
        user.setId(source.get("id", Long.class));
        user.setUserName(source.get("user_name", String.class));
        user.setAddress(source.get("address", String.class));
        user.setAge(source.get("age", int.class));
        user.setDeleted(source.get("deleted", Integer.class) == 1);
        user.setVersion(source.get("version", Long.class));
        return user;
    }
}

package cn.fzkj.controller;

import cn.fzkj.pojo.User;
import cn.fzkj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.Part;
import org.springframework.http.codec.multipart.PartEvent;
import org.springframework.http.codec.multipart.PartEventHttpMessageReader;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("byname")
    public Mono<User> findUserByName(String name){
//    public Mono<User> findUserByName(String name, @RequestBody Flux<PartEvent> data, ServerRequest request){
//        Flux<PartEvent> allPartEvents = request.bodyToFlux(PartEvent.class);
//        allPartEvents.windowUntil()

        return service.findUserByName(name);
    }

    @GetMapping("byaddress")
    public Flux<User> findUserByAddress(String address){
        return service.findUserByAddress(address);
    }

    @PostMapping
    public Mono<User> save(@RequestBody User user) {
        return service.save(user);
    }

    @GetMapping("/{id}")
    public Mono<User> findById(@PathVariable Long id) {
        return service.findById(id);
    }

}

package cn.fzkj;

import cn.fzkj.pojo.User;
import cn.fzkj.repository.UserRepository;
import cn.fzkj.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Map;

@SpringBootTest
class SpringbootReactorApplicationTests {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;

    @Test
    void contextLoads() {
    }

    @Test
    public void test()
    {
        for (int i = 1; i < 2; i++) {
            User user1 = new User();
            user1.setUserName("user-" + i);
            user1.setAddress("四川省成都市武侯区南湖街道-安公路 " + i + " 段" );
            user1.setAge(i);
            user1.setVersion(1L);
            userRepository.save(user1)
                    .block();
        }
    }

    @Test
    public void tetrace() {
        WebClient webClient = WebClient.builder().build();
        Map block = webClient.get().uri("http://101.35.196.199:8090/api/admin/is_installed")
                .retrieve().bodyToMono(Map.class).block();
        Object data = block.get("data");
        System.out.println(data);
    }

}

package cn.fzkj.mqtt;

import cn.fzkj.mqtt.component.MqttComponent;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MqttApplicationTests {

    @Autowired
    MqttComponent component;

    @Test
    void contextLoads() {
        component.publish(1, false, "topic1", "hello world");
    }

}

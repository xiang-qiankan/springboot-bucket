package cn.fzkj.api;

import cn.fzkj.pojo.ResourceItem;
import com.alibaba.fastjson.JSONObject;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Component
public class ElasticSearchClient {

    @Autowired
    private RestHighLevelClient client;

    public boolean createIndex(String indexName) throws IOException {
        CreateIndexRequest request = new CreateIndexRequest(indexName);
        CreateIndexResponse response = client.indices().create(request, RequestOptions.DEFAULT);
        return response.isAcknowledged();
    }

    public boolean deleteIndex(String indexName) throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest(indexName);
        AcknowledgedResponse response = client.indices().delete(request, RequestOptions.DEFAULT);
        return response.isAcknowledged();
    }

    public String[] index() throws IOException {
        GetIndexRequest request = new GetIndexRequest();
        GetIndexResponse response = client.indices().get(request, RequestOptions.DEFAULT);
        return response.getIndices();
    }

    public ResourceItem getDocument(String indexName, String id) throws IOException {
        GetRequest request = new GetRequest();
        request.index(indexName)
                .id(id);
        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        String result = response.getSourceAsString();

        return JSONObject.parseObject(result, ResourceItem.class);
    }

    public boolean deleteDocument(String indexName, String id) throws IOException {
        DeleteRequest request = new DeleteRequest(indexName, id);
        DeleteResponse response = client.delete(request, RequestOptions.DEFAULT);
        return response.status().equals(RestStatus.OK);
    }

    public boolean indexDocument(String indexName, ResourceItem item) throws IOException {
        IndexRequest request = new IndexRequest();
        if (StringUtils.hasLength(item.getId())){
            request.id(item.getId());
        }
        request.index(indexName)
                .source(JSONObject.toJSONString(item), XContentType.JSON);

        IndexResponse response = client.index(request, RequestOptions.DEFAULT);
        return response.status() == RestStatus.CREATED || response.status() == RestStatus.OK;
    }

    public boolean updateDocument(String indexName, String id, ResourceItem item) throws IOException {
        XContentBuilder builder = getxContentBuilder(item);
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index(indexName)
                .id(id)
                .doc(builder);
        UpdateResponse response = client.update(updateRequest, RequestOptions.DEFAULT);
        return response.status() == RestStatus.OK;
    }

    private static XContentBuilder getxContentBuilder(ResourceItem item) throws IOException {
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        if (StringUtils.hasText(item.getResourceId())){
            builder.field("resourceId", item.getResourceId());
        }
        if (StringUtils.hasText(item.getResourceName())){
            builder.field("resourceName", item.getResourceName());
        }
        if (StringUtils.hasText(item.getDescription())){
            builder.field("description", item.getDescription());
        }
        if (item.getResourceType() != null){
            builder.field("resourceType", item.getResourceType());
        }
        if (StringUtils.hasText(item.getUserName())){
            builder.field("userName", item.getUserName());
        }
        if (StringUtils.hasText(item.getAddress())){
            builder.field("address", item.getAddress());
        }
        if (item.getBirthday() != null){
            builder.field("birthday", item.getBirthday().getTime());
        }
        if (item.getAge() != null){
            builder.field("age", item.getAge());
        }
        builder.endObject();
        return builder;
    }

}

package cn.fzkj;

import cn.fzkj.api.ElasticSearchClient;
import cn.fzkj.pojo.ResourceItem;
import cn.fzkj.pojo.ResourceType;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
class ElasticsearchApplicationTests {
    @Autowired
    private ElasticSearchClient client;

    String INDEX_NAME = "ut_es_index_elasticsearchapplicationtests_user";
    String id = "20231121133300003";

    @BeforeEach
    public void init() throws IOException {
        // 创建索引
        boolean status = client.createIndex(INDEX_NAME);
        Assert.assertTrue(status);
    }

    @AfterEach
    public void destory() throws IOException {
        // 删除索引
        boolean status = client.deleteIndex(INDEX_NAME);
        Assert.assertTrue(status);
    }

    @Test
    public void testIndexUser() throws IOException {
        ResourceItem item = new ResourceItem();
        item.setId(id);
        item.setUserName("yanghuanxi");
        item.setResourceId("00003");
        item.setAge(18);
        item.setAddress("北京东燕郊");
        item.setBirthday(new Date());
        item.setResourceType(ResourceType.USER);
        item.setDescription("测试索引文档");
        boolean status = client.indexDocument(INDEX_NAME, item);
        Assert.assertTrue(status);
        // get
        ResourceItem res = client.getDocument(INDEX_NAME, id);
        Assert.assertEquals(item.getUserName(), res.getUserName());
        // update
        ResourceItem item1 = new ResourceItem();
        item1.setUserName("yanghuanxi111");
        item1.setResourceId("00003");
        item1.setAge(2811);
        item1.setAddress("河北廊坊三河燕郊");
        item1.setBirthday(new Date());
        item1.setResourceType(ResourceType.SERVICE);
        item1.setDescription("测试修改索引文档111");
        status = client.updateDocument(INDEX_NAME, id, item1);
        Assert.assertTrue(status);
        // get
        ResourceItem res1 = client.getDocument(INDEX_NAME, id);
        Assert.assertEquals(item1.getUserName(), res1.getUserName());
        // delete:document
        status = client.deleteDocument(INDEX_NAME, id);
        Assert.assertTrue(status);
    }

}

package com.xncoding.pos.dynamic;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ISysJobRepository extends JpaRepository<SysJobPO, Long> {

    @Query(value = "select j from SysJobPO j where j.jobStatus = ?1")
    List<SysJobPO> getSysJobListByStatus(int status);

    List<SysJobPO> findSysJobPOByJobStatus(int status);
}

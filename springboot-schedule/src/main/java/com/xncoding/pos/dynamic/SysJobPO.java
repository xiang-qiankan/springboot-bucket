package com.xncoding.pos.dynamic;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_job")
public class SysJobPO {
    /**
     * 任务ID
     */
    @Column(name = "jobId")
    private Integer jobId;
    /**
     * bean名称
     */
    @Column(name = "beanName")
    private String beanName;
    /**
     * 方法名称
     */
    @Column(name = "methodName")
    private String methodName;
    /**
     * 方法参数
     */
    @Column(name = "methodParams")
    private String methodParams;
    /**
     * cron表达式
     */
    @Column(name = "cron")
    private String cron;
    /**
     * 状态（1正常 0暂停）
     */
    @Column(name = "jobStatus")
    private Integer jobStatus;
    /**
     * 创建时间
     */
    @Column(name = "createTime")
    private Date createTime;
    /**
     * 更新时间
     */
    @Column(name = "updateTime")
    private Date updateTime;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getMethodParams() {
        return methodParams;
    }

    public void setMethodParams(String methodParams) {
        this.methodParams = methodParams;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public Integer getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(Integer jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}

package com.xncoding.pos.controller;

import com.xncoding.pos.dynamic.ISysJobRepository;
import com.xncoding.pos.dynamic.SysJobPO;
import com.xncoding.pos.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping
@RestController
public class IndexController {
    @Autowired
    private JobService jobService;

    @PostMapping
    public void save(@RequestBody SysJobPO job){
        jobService.save(job);
    }

    @PutMapping
    public void update(@RequestBody SysJobPO job){
        jobService.save(job);
    }

    @DeleteMapping
    public void delete(Long id){
        jobService.delete(id);
    }
}

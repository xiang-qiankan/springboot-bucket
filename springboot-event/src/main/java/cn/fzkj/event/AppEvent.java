package cn.fzkj.event;


import org.springframework.context.ApplicationEvent;

public class AppEvent extends ApplicationEvent {

    private User user;
    private Integer type;

    public AppEvent(Object source, User user, Integer type) {
        super(source);
        this.user = user;
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "AppEvent{" +
                "user=" + user +
                ", type=" + type +
                '}';
    }
}

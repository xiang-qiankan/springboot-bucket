package cn.fzkj.service;

import cn.fzkj.event.AppEvent;
import cn.fzkj.event.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class PublisherService {

    @Autowired
    private ApplicationEventPublisher publisher;


    public void publishEvent(String name, String address){
        publisher.publishEvent(new AppEvent(this, new User(111L, name, address), 1));
    }
}

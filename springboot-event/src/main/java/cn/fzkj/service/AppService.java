package cn.fzkj.service;

import cn.fzkj.event.AppEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class AppService {

    @EventListener(value = AppEvent.class)
    public void handleEvent(AppEvent event) {
        System.out.println("AppService.handleEvent");
        System.out.println(event.getUser());
        System.out.println(event.getType());
    }
}

package cn.fzkj.listener;

import cn.fzkj.event.AppEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class AppEventListener implements ApplicationListener<AppEvent> {

    @Override
    public void onApplicationEvent(AppEvent event) {
//        System.out.println(event);
    }
}

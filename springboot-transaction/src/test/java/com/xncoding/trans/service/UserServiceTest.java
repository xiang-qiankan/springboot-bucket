package com.xncoding.trans.service;

import com.xncoding.trans.service.impl.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;


@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

//    @Autowired
//    private UserService userService;

    @Autowired
    private IUserService iUserService;

    Map<String , Object> map = new ConcurrentHashMap<>();

    @Test
    public void getById() {
//        System.out.println(userService.getById(1));
        System.out.println(map.get("1"));
    }

    @Test
    public void getUserByName(){
//        Thread thread = new Thread(() -> {
//            userService.getUser1(Thread.currentThread().getName());
//        });
//        thread.start();
//
//        userService.getUser1(Thread.currentThread().getName());
//        try {{
////        Thread thread = new Thread(() -> {
////            userService.getUser1(T
//            thread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    @Test
    public void getUser(){
//        Thread thread = new Thread(() -> {
//            userService.getUserBy(Thread.currentThread().getName());
//        });
//        thread.start();
//
//        userService.getUserBy(Thread.currentThread().getName());
//        try {
//            thread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    @Test
    public void updateUserError() {
    }

    @Test
    public void updateUserError2() {
    }

    // 1、外层加事务，内层不加事务，内层抛异常会不会回滚【会回滚】
    @Test
    public void testTransactionFailureCase(){
//        userService.updateUserById(1);
    }

    // 2、外层加事务，内层加事务，内层抛异常会不会回滚【会】
    @Test
    public void testTransactionFailureCase2(){
//        userService.updateUserById1(1);
    }

    // 3、外层不加事务，内层加事务，内层抛异常会不会回滚【不会】
    @Test
    public void testTransactionFailureCase3(){
//        userService.updateUserById2(1);
    }

    /**
     * 外层加事务，内层加事务但是是实现类的方法，接口中没有定义，会不会回滚【不会】
     */
    @Test
    public void test(){
//        iUserService.updateUserById1(1);
    }
}
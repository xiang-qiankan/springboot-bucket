package com.xncoding.trans.service.impl;

import com.xncoding.trans.dao.entity.User;

/**
 * @DESCRIPTION
 * @Author yaya
 * @DATE 2023/4/6
 */
public interface IUserService {
    void updateUserById(Integer id);

    void updateUserById1(Integer id);

    void updateUserById2(Integer id);

    Integer getUserWithTransaction2(String name);

    User getById(int id);

    void updateUser(User user);

    void updateUserError(User user);

    void updateUserError2(User user);


}

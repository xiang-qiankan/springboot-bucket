package cn.fzkj.controller;

import cn.fzkj.handler.MergeColumnHandler;
import cn.fzkj.vo.CrTestExportVO;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ author mr
 * @ description
 * @ since 2024/10/10
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    @GetMapping("export")
    public void exportClassroomTestRecord(HttpServletResponse response)
            throws IOException {
        List<CrTestExportVO> list = build();

        String projectName = "DEMO";
        response.setContentType("application/vnd.ms-excel");
        String var10002 = new String("demo".getBytes("gbk"), "iso8859-1");
        response.addHeader("Content-disposition", "attachment;filename=" + var10002 + ".xls");
        MergeColumnHandler mergeColumnHandler = new MergeColumnHandler(1, new int[] {0, 1, 2, 3});
        EasyExcel.write(response.getOutputStream(), CrTestExportVO.class).registerWriteHandler(mergeColumnHandler)
                .sheet("sheet1").head(getHead("大标题名称:" + "【" + projectName + "】")).doWrite(list);
    }

    private List<List<String>> getHead(String projectName) {
        List<List<String>> list = new ArrayList<>();
        Class<CrTestExportVO> crTestExportVOClass = CrTestExportVO.class;
        List<Field> fields = Arrays.stream(crTestExportVOClass.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(ExcelProperty.class)).toList();
        for (Field field : fields) {
            ExcelProperty annotation = field.getAnnotation(ExcelProperty.class);
            String[] value = annotation.value();
            if (value.length == 1) {
                list.add(Arrays.asList(projectName, value[0]));
            }
        }
        return list;
    }

    private List<CrTestExportVO> build(){
        List<CrTestExportVO> list = new ArrayList<>();
        CrTestExportVO c1 = new CrTestExportVO("是否成年", 1, "", "", "成年", 2, "100.00%");
        CrTestExportVO c2 = new CrTestExportVO("是否成年", 1, "", "", "未成年", 0, "0.00%");
        CrTestExportVO c3 = new CrTestExportVO("职业", 1, "", "", "学生", 1, "50.00%");
        CrTestExportVO c4 = new CrTestExportVO("职业", 1, "", "", "大堂经理", 1, "50.00%");
        CrTestExportVO c5 = new CrTestExportVO("职业", 1, "", "", "其他", 0, "0.00%");
        CrTestExportVO c6 = new CrTestExportVO("建筑施工企业应当依法为职工参加（　A　）缴纳保险费。", 1, "0.00%", "A", "工伤保险", 1, "50.00%");
        CrTestExportVO c7 = new CrTestExportVO("建筑施工企业应当依法为职工参加（　A　）缴纳保险费。", 1, "0.00%", "A", "意外保险", 0, "0.00%");
        CrTestExportVO c8 = new CrTestExportVO("建筑施工企业应当依法为职工参加（　A　）缴纳保险费。", 1, "0.00%", "A", "人身保险", 1, "50.00%");
        CrTestExportVO c9 = new CrTestExportVO("（　B　）应当按照国家规定建立国家综合性消防救援队、专职消防队，并按照国家标准配备消防装备，承担火灾扑救工作。", 1, "50.00%", "B", "乡镇级以上地方人民政府", 1, "50.00%");
        CrTestExportVO c10 = new CrTestExportVO("（　B　）应当按照国家规定建立国家综合性消防救援队、专职消防队，并按照国家标准配备消防装备，承担火灾扑救工作。", 1, "50.00%", "B", "县级以上地方人民政府", 1, "50.00%");
        list.add(c1);
        list.add(c2);
        list.add(c3);
        list.add(c4);
        list.add(c5);
        list.add(c6);
        list.add(c7);
        list.add(c8);
        list.add(c9);
        list.add(c10);
        return list;
    }

}

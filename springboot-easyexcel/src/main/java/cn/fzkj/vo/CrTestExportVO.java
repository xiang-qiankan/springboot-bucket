package cn.fzkj.vo;

import cn.fzkj.convert.DictConverter;
import cn.fzkj.convert.DictFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

import java.io.Serializable;

/**
 * @ author mr
 * @ description
 * @ since 2024/10/8
 */
@Data
@ExcelIgnoreUnannotated
public class CrTestExportVO implements Serializable {

    @ColumnWidth(100)
    @ExcelProperty(value = "题目名称")
    private String questionName;

    @ExcelProperty(value = "题型")
//    @ExcelProperty(value = "题型", converter = DictConverter.class)
    @DictFormat("questionType")
    private Integer type;

    @ExcelProperty(value = "正确率")
    private String correctRate;

    @ExcelProperty(value = "正确答案")
    private String correctAnswer;

    @ColumnWidth(30)
    @ExcelProperty(value = "选项")
    private String option;

    // A B C D
    private String option1;

    @ExcelProperty(value = "小计")
    private Integer subtotal;

    @ExcelProperty(value = "比例")
    private String proportion;

    public CrTestExportVO(String questionName, Integer type, String correctRate, String correctAnswer, String option, Integer subtotal, String proportion) {
        this.questionName = questionName;
        this.type = type;
        this.correctRate = correctRate;
        this.correctAnswer = correctAnswer;
        this.option = option;
        this.subtotal = subtotal;
        this.proportion = proportion;
    }

    public CrTestExportVO(String questionName, Integer type, String correctRate, String correctAnswer, String option, String option1, Integer subtotal, String proportion) {
        this.questionName = questionName;
        this.type = type;
        this.correctRate = correctRate;
        this.correctAnswer = correctAnswer;
        this.option = option;
        this.option1 = option1;
        this.subtotal = subtotal;
        this.proportion = proportion;
    }
}

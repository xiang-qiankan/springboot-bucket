package cn.fzkj.convert;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.metadata.data.WriteCellData;

/**
 * @ author mr
 * @ description
 * @ since 2024/10/10
 */
public class DictConverter implements Converter<Object> {

    //

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Object> context) throws Exception {
        // todo 配合DictFormat注解使用，去数据库中查询字典的值，在导出excel的时候将int类型转换为字符串导出
        return Converter.super.convertToExcelData(context);
    }
}

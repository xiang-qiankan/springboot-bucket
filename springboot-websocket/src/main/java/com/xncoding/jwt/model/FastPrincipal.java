package com.xncoding.jwt.model;

import java.security.Principal;

public class FastPrincipal implements Principal {

    private final String name;

    public FastPrincipal(String name){
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}

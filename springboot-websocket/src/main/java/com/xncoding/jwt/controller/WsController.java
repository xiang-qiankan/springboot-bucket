package com.xncoding.jwt.controller;

import com.xncoding.jwt.model.Message;
import com.xncoding.jwt.model.RequestMessage;
import com.xncoding.jwt.model.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * WsController
 *
 * @author XiongNeng
 * @version 1.0
 * @since 2018/2/28
 */
@Controller
public class WsController {

    private final SimpMessagingTemplate messagingTemplate;

    @Autowired
    public WsController(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @MessageMapping("/message")
    @SendTo("/topic/say")
    public ResponseMessage say(Message message, Principal principal) {
        System.out.println(principal.getName());
        messagingTemplate.convertAndSendToUser(message.getTo(), "/queue", new Message(message.getMessage(), message.getDatetime(), message.getFrom(), message.getTo()));
        System.out.println(principal.getName() + "发送了一条消息给：" + message.getTo());
        return new ResponseMessage("welcome," + principal.getName() + " !");
    }

    @SubscribeMapping("/say")
    public ResponseMessage sub() {
        ResponseMessage responseMessage = new ResponseMessage("感谢你订阅了我");
        return responseMessage;
    }

    /**
     * 定时推送消息
     */
    @Scheduled(fixedRate = 5000)
    public void callback() {
        // 发现消息
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        messagingTemplate.convertAndSend("/topic/callback", "定时推送消息时间: " + df.format(new Date()));
    }
}

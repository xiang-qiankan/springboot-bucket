package com.xncoding.service;

import com.xncoding.Application;
import com.xncoding.pos.mq.OrderCancelReceiver;
import com.xncoding.pos.service.OrderSenderService;
import com.xncoding.pos.service.SenderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


/**
 * SenderServiceTest
 *
 * @author XiongNeng
 * @version 1.0
 * @since 2018/2/2
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class SenderServiceTest {
    @Autowired
    private SenderService senderService;
    @Autowired
    private OrderSenderService orderSenderService;

    @Autowired
    private OrderCancelReceiver orderCancelReceiver;

    @Test
    public void testCache() {
        // 测试广播模式
        senderService.broadcast("同学们集合啦！");
        // 测试Direct模式
        senderService.direct("定点消息");
    }

    @Test
    public void testDl(){
        orderSenderService.sendOrderCreate();
    }
}

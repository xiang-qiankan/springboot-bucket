package com.xncoding.pos.constant;

/**
 * 定义一些常量
 */
public class MQConstant {

    // 订单死信交换机
    public static final String ORDER_DELAY_EXCHANGE = "order_delay_exchange";
    // 订单创建交换机
    public static final String ORDER_CREATE_EXCHANGE = "order_create_exchange";
    // 订单死信队列
    public static final String ORDER_DELAY_QUEUE = "order_delay_queue";
    // 订单创建队列
    public static final String ORDER_CREATE_QUEUE = "order_create_queue";
    // 订单路由
    public static final String ORDER_ROUTE_KEY = "order_route_key";
    // 死信队列路由
    public static final String CANCEL_DL_ROUTE_KEY = "cancel_dl_route_key";

    public static final String DLE = "x-dead-letter-exchange";
    public static final String DLK = "x-dead-letter-routing-key";
    public static final String TTL = "x-message-ttl";
}

package com.xncoding.pos.mq;

import com.rabbitmq.client.Channel;
import com.xncoding.pos.constant.MQConstant;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

@Component
public class OrderCancelReceiver {

    @RabbitListener(queues = {MQConstant.ORDER_DELAY_QUEUE})
    public void cancelOrder(Message message, Channel channel) throws IOException {
        System.out.println(new String (message.getBody()));
        System.out.println(new Date());

        channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
    }
}

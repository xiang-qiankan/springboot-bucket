package com.xncoding.pos.service;

import com.xncoding.pos.constant.MQConstant;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OrderSenderService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendOrderCreate(){
        rabbitTemplate.convertAndSend(MQConstant.ORDER_CREATE_EXCHANGE, MQConstant.ORDER_ROUTE_KEY, "heihei");
        System.out.println(new Date());
    }
}
